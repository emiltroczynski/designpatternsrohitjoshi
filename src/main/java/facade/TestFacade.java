package facade;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public class TestFacade {
    public static void main(String[] args) {
        ScheduleServer scheduleServer = new ScheduleServer();
        ScheduleServerFacade scheduleServerFacade = new ScheduleServerFacade(scheduleServer);
        scheduleServerFacade.startServer();

        System.out.println("---Start working......");
        System.out.println("---After work done.........");

        scheduleServerFacade.stopServer();
    }
}
