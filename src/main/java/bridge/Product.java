package bridge;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public interface Product {
    public String productName();

    public void produce();
}
