package bridge;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public class GearLocking implements Product {
    private final String productName;

    public GearLocking(String productName) {
        this.productName = productName;
    }

    @Override
    public String productName() {
        return productName;
    }

    @Override
    public void produce() {
        System.out.println("Producing Gear Locking System");
    }
}
