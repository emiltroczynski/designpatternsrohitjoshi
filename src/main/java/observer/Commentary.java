package observer;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public interface Commentary {
    public void setDesc(String desc);
}
