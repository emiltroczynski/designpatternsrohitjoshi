package observer;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public interface Subject {
    public void subscribeObserver(Observer observer);

    public void unSubscribeObserver(Observer observer);

    public void notifyObservers();

    public String subjectDetails();
}
