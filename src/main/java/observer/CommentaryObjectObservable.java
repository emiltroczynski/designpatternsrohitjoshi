package observer;

import java.util.Observable;

/**
 * Created by Emil_Troczynski on 07/07/2017.
 */
public class CommentaryObjectObservable extends Observable implements Commentary {
    private final String subjectDetails;
    private String desc;

    public CommentaryObjectObservable(String subjectDetails) {
        this.subjectDetails = subjectDetails;
    }

    @Override
    public void setDesc(String desc) {
        this.desc = desc;
        setChanged();
        notifyObservers(desc);
    }

    public String subjectDetails() {
        return subjectDetails;
    }
}
