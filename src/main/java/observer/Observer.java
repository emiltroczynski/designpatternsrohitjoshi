package observer;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public interface Observer {
    public void update(String desc);

    public void subscribe();

    public void unSubscribe();
}
