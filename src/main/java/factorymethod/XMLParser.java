package factorymethod;

public interface XMLParser {
  String parse();
}