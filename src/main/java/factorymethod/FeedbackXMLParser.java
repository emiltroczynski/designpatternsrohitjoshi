package factorymethod;

public class FeedbackXMLParser implements XMLParser {
  @Override
  public String parse() {
    System.out.println("Parsing XML..");
    return "Feedback XML Message";
  }
}
