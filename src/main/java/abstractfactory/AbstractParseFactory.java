package abstractfactory;

public interface AbstractParseFactory {
  XMLParser getParserInstance(String parserType);
}
