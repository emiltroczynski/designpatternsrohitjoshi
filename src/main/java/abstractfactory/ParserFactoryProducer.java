package abstractfactory;

public class ParserFactoryProducer {
  private ParserFactoryProducer() {
    throw new AssertionError();
  }

  public static AbstractParseFactory getFactory(String factoryType) {
    switch (factoryType) {
      case "NYFactory":
        return new NYParserFactory();
      case "TWFactory":
        return new TWParseFactory();

    }
    return null;
  }

}
