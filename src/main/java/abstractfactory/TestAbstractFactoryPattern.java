package abstractfactory;

public class TestAbstractFactoryPattern {
  public static void main(String[] args) {
    AbstractParseFactory parseFactory = ParserFactoryProducer.getFactory("NYFactory");
    XMLParser parser = parseFactory.getParserInstance("NYORDER");
    String msg = "";
    msg = parser.parse();
    System.out.println(msg);

    System.out.println("************************************");

    parseFactory = ParserFactoryProducer.getFactory("TWFactory");
    parser = parseFactory.getParserInstance("TWFEEDBACK");
    msg = parser.parse();
    System.out.println(msg);

  }
}
