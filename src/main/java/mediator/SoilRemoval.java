package mediator;

/**
 * Created by Emil_Troczynski on 07/07/2017.
 */
public class SoilRemoval {
    public void low() {
        System.out.println("Setting Soil Removal to low");
    }

    public void medium() {
        System.out.println("Setting Soil Removal to medium");
    }

    public void high() {
        System.out.println("Setting Soil Removal to high");
    }
}
