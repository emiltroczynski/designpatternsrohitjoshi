package mediator;

/**
 * Created by Emil_Troczynski on 07/07/2017.
 */
public class Sensor {
    public boolean checkTemperature(int temp) {
        System.out.println("Temperature reached " + temp + "*C");
        return true;
    }
}
