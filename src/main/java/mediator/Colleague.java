package mediator;

/**
 * Created by Emil_Troczynski on 07/07/2017.
 */
public interface Colleague {
    public void setMediator(MachineMediator mediator);
}
