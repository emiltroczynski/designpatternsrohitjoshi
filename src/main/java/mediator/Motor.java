package mediator;

/**
 * Created by Emil_Troczynski on 07/07/2017.
 */
public class Motor {
    public void startMotor() {
        System.out.println("Start motor...");
    }

    public void rotateDrum(int rpm) {
        System.out.println("Rotating drum at " + rpm + " rpm.");
    }
}
