package composite;

/**
 * Created by Emil_Troczynski on 06/07/2017.
 */
public class TestCompositePattern {
    public static void main(String[] args) {
        HtmlTag rootTag = new HtmlParentElement("<html>");
        rootTag.setStartTag("<html>");
        rootTag.setEndTag("</html>");

        HtmlTag rootChild = new HtmlParentElement("<body>");
        rootChild.setStartTag("<body>");
        rootChild.setEndTag("</body>");
        rootTag.addChildTag(rootChild);

        HtmlTag child1 = new HtmlElement("<P>");
        child1.setStartTag("<P>");
        child1.setEndTag("</P>");
        child1.setTagBody("Testing html tag library");
        rootChild.addChildTag(child1);

        child1 = new HtmlElement("<P>");
        child1.setStartTag("<P>");
        child1.setEndTag("</P>");
        child1.setTagBody("Paragraph 2");
        rootChild.addChildTag(child1);

        HtmlTag child2 = new HtmlElement("<P2>");
        child2.setStartTag("<P2>");
        child2.setEndTag("</P2>");
        rootChild.addChildTag(child2);

        rootTag.generateHtml();
    }
}
